@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            <div class="form-group">
                                
                                <label>Package Name</label>

                                <input type="text" name="package_name" class="form-control" required>

                            </div>



                            <div class="form-group">
                                
                                <label>Venue</label>

                                <select name="venue" class="form-control select2">

                                <option value="N/A">N/A</option>

                                @foreach (App\Venue::all() as $item)
                                    <option value="{{$item->venue_name}}">{{$item->venue_name}}</option>
                                @endforeach

                                </select>

                            </div>


                            <div class="form-group">
                            
                                <label class="control-label" for="name">Menus</label>
                                <select class="form-control select2-taggable" name="menus[]" multiple="" data-method="add" data-label="name" data-error-message="Sorry it appears there may have been a problem creating the record. Please make sure your table has defaults for other fields.">
                                    
                                    @foreach (App\MenuDish::all() as $item)
                                        <option value="{{$item->menu_name}}">{{$item->menu_name}}</option>
                                    @endforeach
                                    
                                </select>
                                
                            </div>

                            <div class="form-group">
                            
                                <label class="control-label" for="name">Services</label>
                                <select class="form-control select2-taggable" name="services[]" multiple="" data-method="add" data-label="name" data-error-message="Sorry it appears there may have been a problem creating the record. Please make sure your table has defaults for other fields.">
                                    
                                    @foreach (App\Service::all() as $item)
                                        <option value="{{$item->service_name}}">{{$item->service_name}}</option>
                                    @endforeach
                                    
                                </select>
                                
                            </div>

                            <div class="form-group">
                            
                                <label class="control-label" for="name">Materials</label>
                                <select class="form-control select2-taggable" name="materials[]" multiple="" data-method="add" data-label="name" data-error-message="Sorry it appears there may have been a problem creating the record. Please make sure your table has defaults for other fields.">
                                    
                                    @foreach (App\Material::all() as $item)
                                        <option value="{{$item->material_name}}">{{$item->material_name}}</option>
                                    @endforeach
                                    
                                </select>
                                
                            </div>









                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                   

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    
@stop
