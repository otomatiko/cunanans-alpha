<!-- Modal -->
<div class="modal fade" id="ingredients_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Create Ingredient</h5>
			</div>
			<div class="modal-body">

				<form id="form" action="/admin/ingredients" method="POST" enctype="multipart/form-data">
				
					@csrf

					<div class="modal-card em11 mb-2">


						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Item Name</label>
							<input type="text" class="form-control" name="item_name">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Description</label>
							<input type="text" class="form-control" name="description">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Unit Of Measurement</label>
							<input type="text" class="form-control" name="unit_of_measurement">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Supplier</label>
							<select class="form-control select2" name="supplier">
								<option value="" disabled selected>Select Supplier</option>
								@foreach($suppliers as $supplier)
								<option>{{$supplier->supplier_name}}</option>
								@endforeach
							</select>
							
						</div>


					</div>

					<div class="modal-card em6">


						<div class="form-group  col-md-12 ">
		
							<label class="control-label" for="name">Current Price</label>
							<input type="text" class="form-control" name="current_price" placeholder="Current Price" value="">
							
						</div>
						
					</div>

				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-blue" onclick="submit_form()">Done</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	function submit_form(){
		$('#form').submit();
	}

</script>