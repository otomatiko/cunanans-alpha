<!-- Modal -->
<div class="modal fade" id="suppliers_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Create Supplier</h5>
			</div>
			<div class="modal-body">

				<form id="form" action="/admin/suppliers" method="POST" enctype="multipart/form-data">
				
					@csrf

					<div class="modal-card em19 mb-2">


						<div class="form-group col-md-12 modal-input">
		
							<label class="control-label" for="name">Supplier Name</label>
							<input type="text" class="form-control" name="supplier_name">
							
							<label class="control-label" for="name">Supplier Address</label>
							<input type="text" class="form-control" name="supplier_address">
							
							<label class="control-label" for="name">Supplier Contact Number</label>
							<input type="text" class="form-control" name="supplier_contact">
							
							<label class="control-label" for="name">Supplier Email</label>
							<input type="text" class="form-control" name="supplier_email">
							
						</div>

					</div>



					<div class="modal-card em6 mb-2">


						<div class="form-group col-md-12 modal-input 6em">
		
							<label class="control-label" for="name">Price History</label>
							<input type="number" class="form-control" name="price_histories_id">
							
						</div>

					</div>

					<div class="modal-card em6">

						<div class="form-group col-md-6 modal-input 6em">

							<label>Product</label>
							<input type="text" class="form-control" name="product">

						</div>

						<div class="form-group col-md-6 modal-input 6em">
							<label>Current Price</label>
							<input type="text" class="form-control" name="current_price">
						</div>

                    </div>


				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-blue" onclick="submit_form()">Done</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

    function submit_form(){
        $('#form').submit();
    }

</script>