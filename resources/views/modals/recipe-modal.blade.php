<!-- Modal -->
<div class="modal fade" id="recipe_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Create Recipe</h5>
			</div>
			<div class="modal-body">

				<form id="form" action="/admin/recipes" method="POST" enctype="multipart/form-data">
				
					@csrf

					<div class="modal-card em6 mb-2">


						<div class="form-group  col-md-12 modal-input 6em">
		
							<label class="control-label" for="name">Recipe Name</label>
							<input type="text" class="form-control" name="recipe_name">
							
						</div>

					</div>

					<div class="modal-card em20">

						<div id="base" style="height: 16em; overflow-y: auto;"> {{-- BASE=================== --}}

	                        <div class="form-group col-md-6">
	                            <label>Ingredient</label>

	                            <select class="form-control" name="ingredient[]">
	                                
	                                 @foreach($ingredients as $item)
	                                    <option value="{{$item->item_name}}">{{$item->item_name}}</option>
	                                @endforeach

	                            </select>


	                        </div>
	                        <div class="form-group col-md-6">
	                            <label>Weight</label>
	                            <input type="text" class="form-control" name="weight[]">
	                        </div>

	                    </div> {{-- end base --}}

	                    <div class="col-md-12" style="">
	                        <a href="#" id="add" class="btn-primary btn"><i class="voyager-plus"></i></a>
	                    </div>

                    </div>


				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-blue" onclick="submit_form()">Done</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
    var html = `
    <div id="container">
        

        <div class="form-group col-md-6">
            <select class="form-control" name="ingredient[]">
                                        
            @foreach($ingredients as $item)
                <option value="{{$item->item_name}}">{{$item->item_name}}</option>
            @endforeach

            </select>
        </div>

        <div class="form-group col-md-5">
            <input type="text" class="form-control" name="weight[]">
        </div>

        <div class="form-group" style="text-align:center;">

            <a href="#" id="remove" style="margin-top:0px" class="btn-danger btn"><i class="voyager-trash"></i></a>
            
        </div>
    
    </div>
    `;
    let count = 0;
    $('#add').click(function(e){
        if(count > 20){
            alert('limit reached');
            exit();
        };
        $('#base').append(html);
        count++;
    });
        
    $('.modal-body').on('click', '#remove', function(e){

        $(this).parent('div').parent('div').remove();
        count--;
    });
    
});

function submit_form(){
	$('#form').submit();
}
</script>