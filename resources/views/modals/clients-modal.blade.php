<!-- Modal -->
<div class="modal fade" id="clients_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Create New Client</h5>
			</div>
			<div class="modal-body">

				<form id="form" action="/admin/clients" method="POST" enctype="multipart/form-data">
				
					@csrf

					<div class="modal-card em20 mb-2">


						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Groom Name</label>
							<input type="text" class="form-control" name="groom_name">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Groom Birthdate</label>
							<input type="date" class="form-control" name="groom_birthdate">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Bride Name</label>
							<input type="text" class="form-control" name="bride_name">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Bride Birthdate</label>
							<input type="date" class="form-control" name="bride_birthdate">
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Contact Information</label>
							<textarea class="form-control" name="contact_info" rows="3"></textarea>
							
						</div>

						<div class="form-group  col-md-6 modal-input">
		
							<label class="control-label" for="name">Wedding Anniversary</label>
							<input type="date" class="form-control" name="wedding_anniv">
							
						</div>



					</div>

				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-blue" onclick="submit_form()">Done</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	function submit_form(){
		$('#form').submit();
	}

</script>