
@php

	$ingredient_names = json_decode($data->ingredient_name);
	$ingredient_weights = json_decode($data->ingredient_weight);

@endphp

<!-- Modal -->
<div class="modal fade" id="recipe_edit_modal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit {{$data->recipe_name}}</h5>
			</div>
			<div class="modal-body">
				<form id="form{{$data->id}}" action="/admin/recipes/{{$data->id}}" method="POST" enctype="multipart/form-data">
					@csrf
					@foreach($ingredient_names as $key => $ingredient_name)
					
					<div id="container">
						<div class="form-group col-md-6">
							<select class="form-control" name="ingredient[]">
								<option value="{{$ingredient_name}}" selected>{{$ingredient_name}}</option>
								@foreach($ingredients as $item)
								<option value="{{$item->item_name}}">{{$item->item_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-5">
							<input type="text" class="form-control" name="weight[]" value="{{$ingredient_weights[$key]}}">
						</div>
						<div class="form-group" style="text-align:center;">
							<a href="#" id="remove{{$data->id}}" style="margin-top:0px" class="btn-danger btn"><i class="voyager-trash"></i></a>
						</div>
					</div>
					@endforeach
					<input type="hidden" name="_method" value="PUT">
					<div class="hidden modal-card em6 mb-2">
						<div class="form-group  col-md-12 modal-input 6em">
							
							<label class="control-label" for="name">Recipe Name</label>
							<input type="text" class="form-control" name="recipe_name" value="{{$data->recipe_name}}">
							
						</div>
					</div>
					<a id="add{{$data->id}}" class="btn-primary btn" style="margin-left: 1em"><i class="voyager-plus"></i></a>
					
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-blue" onclick="submit_form{{$data->id}}()">Done</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
    var html = `
    <div id="container">
        

        <div class="form-group col-md-6">
            <select class="form-control" name="ingredient[]">
                                        
            @foreach($ingredients as $item)
                <option>{{$item->item_name}}</option>
            @endforeach

            </select>
        </div>

        <div class="form-group col-md-5">
            <input type="text" class="form-control" name="weight[]" placeholder="weight">
        </div>

        <div class="form-group" style="text-align:center;">

            <a href="#" id="remove{{$data->id}}" style="margin-top:0px" class="btn-danger btn"><i class="voyager-trash"></i></a>
            
        </div>
    
    </div>
    `;
    let count = 0;
    $('#add{{$data->id}}').click(function(e){
    	e.preventDefault();
        if(count > 20){
            alert('limit reached');
            exit();
        };
        $('#form{{$data->id}}').append(html);
        count++;
    });
        
    $('.modal-body').on('click', '#remove{{$data->id}}', function(e){

        $(this).parent('div').parent('div').remove();
        count--;
    });
    
});

function submit_form{{$data->id}}(){
	$('#form{{$data->id}}').submit();
}
</script>