<!-- Modal -->
<div class="modal fade" id="menu_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Create Menu</h5>
			</div>
			<div class="modal-body">

				<form id="form" action="/admin/menu-dishes" method="POST" enctype="multipart/form-data">
				
					@csrf
					
					<div class="modal-card 11em">


						<div class="panel-body">

                            <div class="form-group">
                                <label>Menu Name</label>
                                <input type="text" class="form-control" name="menu_name">
                            </div>

                            <div class="form-group">
                            
                                <label class="control-label" for="name">Main Dishes</label>
                                <select class="form-control select2-taggable" name="main_dishes[]" multiple="" data-method="add" data-label="name" data-error-message="Sorry it appears there may have been a problem creating the record. Please make sure your table has defaults for other fields.">
                                    
                                    @foreach (App\Dish::where('category', '=', 'main')->get() as $item)
                                        <option value="{{$item->dish_name}}">{{$item->dish_name}}</option>
                                    @endforeach
                                    
                                </select>
                                    
                            </div>

                            <div class="form-group">
                            
                                <label class="control-label" for="name">Side Dishes</label>
                                <select class="form-control select2-taggable" name="side_dishes[]" multiple="" data-method="add" data-label="name" data-error-message="Sorry it appears there may have been a problem creating the record. Please make sure your table has defaults for other fields.">
                                    
                                    @foreach (App\Dish::where('category', '=', 'sidedish')->get() as $item)
                                        <option value="{{$item->dish_name}}">{{$item->dish_name}}</option>
                                    @endforeach
                                    
                                </select>
                                
                            </div>



                        </div>
						
					</div>

				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-blue" onclick="submit_form()">Done</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	function submit_form(){
	$('#form').submit();
	}

</script>