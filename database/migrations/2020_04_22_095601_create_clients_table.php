<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('groom_name')->nullable();
			$table->date('groom_birthdate')->nullable();
			$table->text('bride_name')->nullable();
			$table->date('bride_birthdate')->nullable();
			$table->date('wedding_anniv')->nullable();
			$table->text('contact_info')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}
