<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('packages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('menus')->nullable();
			$table->text('services')->nullable();
			$table->text('materials')->nullable();
			$table->text('venue')->nullable();
			$table->text('total_cost')->nullable();
			$table->timestamps();
			$table->text('menus_total_unit_cost')->nullable();
			$table->text('menus_total_unit_price')->nullable();
			$table->text('services_total_cost')->nullable();
			$table->text('materials_total_cost')->nullable();
			$table->text('venue_total_cost')->nullable();
			$table->text('package_name')->nullable();
			$table->text('materials_total_price')->nullable();
			$table->text('venue_total_price')->nullable();
			$table->text('services_total_price')->nullable();
			$table->text('total_price')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('packages');
	}

}
