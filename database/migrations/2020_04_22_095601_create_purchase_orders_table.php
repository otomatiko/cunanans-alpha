<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchaseOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchase_orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('package')->nullable();
			$table->text('location')->nullable();
			$table->text('venue_caterers_fee')->nullable();
			$table->text('types_services')->nullable();
			$table->text('types_occasions')->nullable();
			$table->text('types_stylings')->nullable();
			$table->time('time')->nullable();
			$table->date('date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('purchase_orders');
	}

}
